from pymongo import MongoClient

# mongodb vars
client = MongoClient('localhost', 27017)
linguisticsDB = client.linguistics
allLanguages = linguisticsDB["languages"].find()
code2name = {}
name2code = {}
for language in list(allLanguages):
    code2name[language["code"]] = language["name"]
    name2code[language["name"]] = language["code"]


def getLanguageName(code):
    try:
        name = code2name[code]
    except KeyError:
        name = code
        pass
    return name


def getLanguageCode(name):
    try:
        code = name2code[name]
    except KeyError:
        code = name
        pass
    return code
