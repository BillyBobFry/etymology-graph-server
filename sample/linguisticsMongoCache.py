from pymongo import MongoClient

MongoClient = MongoClient('localhost', 27017)
cacheDB = MongoClient.cache
freetextCache = cacheDB["etymologyWords"]

def doubletsAutocomplete(language, searchTerm):
    print(searchTerm)

    # first check the cache
    firstTwoCharacters = searchTerm[0:2]
    cachedResult = list(
        freetextCache.find(
            {
                "searchTerm": firstTwoCharacters,
                "languageCode": language,
            }, {"_id": 0}))

    cachedDoublets = list(filter(lambda e: e["doublets"]>0, cachedResult[0]["nodes"]))


    return cachedDoublets