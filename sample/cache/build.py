# build doublets cache
languagesToCache = ["en"]
# for language in languagesToCache:


from neo4j import GraphDatabase
import json
import time
from datetime import datetime
import array
import string
import re
from pymongo import MongoClient

uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)

compoundQueryText = f"""match p=(a:word)<-[r:suffix|affix|compound|prefix|confix]-(b:word)
     where id(a)=$nodeID
     return nodes(p), relationships(p)
    """

doubletQueryText = """
    match (a:word)<-[r:inherited|borrowed|derived *..7]-(b:word) 
            where id(a)=$nodeID
            with collect(distinct b) as nodes 
            unwind nodes as node 
            match p=(c:word)-[q:inherited|derived|borrowed *..7]->(d:word) 
            where d.languageCode=$language 
            and id(c)=id(node) 
            with distinct d as dis
            return count(dis) as count;
    """

numberOfParentsQueryText = """
    match (a:word)<-[r:inherited|borrowed|derived]-(b:word)  
            where id(a)=$nodeID
            with distinct b as dis
            return count(dis) as count;
    """

def nodeIsCompound(node):
    if " " in node["latinTranscription"] or "-" in node["latinTranscription"]:
        return True
    with driver.session() as session:
        bindVars = {
            "nodeID": node["id"]
        }
        linksFromNode = session.run(compoundQueryText, **bindVars)
        g = linksFromNode.graph()
        return len(g.relationships) > 0
        # linkTypes = map(lambda r: r.type, g.relationships)

def getNumberOfDoublets(node):
    with driver.session() as session:
        bindVars = {
            "nodeID": node["id"],
            "language": node.get("languageCode")
        }
        linksFromNode = session.run(doubletQueryText, **bindVars)

        count = 0
        for item in linksFromNode:
            if item.get("count") and item.get("count") > 0:
                count = item.get("count")

        return count

def getNumberOfParents(node):
    with driver.session() as session:
        bindVars = {
            "nodeID": node["id"]
        }
        linksFromNode = session.run(numberOfParentsQueryText, **bindVars)

        count = 0
        for item in linksFromNode:
            if item.get("count") and item.get("count") > 0:
                count = item.get("count")

        return count

def refreshCache():
    client = MongoClient('localhost', 27017)
    db = client.cache
    collection = db["etymologyWords"]

    # empty the collection
    collection.drop()

    roots = []

    for letter1 in string.ascii_lowercase[:26]:
        print(letter1)
        for letter2 in string.ascii_lowercase[:26]:
            thisRoot = letter1+letter2
            print(letter1 + letter2)
            # roots.append(thisRoot)
            searchTerm = thisRoot
            languageCode = "en"
            returnCompounds = False

            with driver.session() as session:
                queryText = """match p=(a:word)
                    where toLower(a.latinTranscription) starts with $searchTerm 
                    and a.languageCode= $languageCode 
                    return nodes(p)"""
                bindVars = {
                    "languageCode": languageCode, "searchTerm": searchTerm
                }
                results = session.run(queryText, **bindVars)
                g = results.graph()
                # print("Here")
                nodes = []
                for n in g.nodes:
                    node = {}
                    node["id"] = n.id
                    # print(n.get("latinTranscription"))
                    # print(list(filter(lambda i: i[0] ==
                    #                   'latinTranscription', n.items()))[0][1])
                    # print()
                    for p in n.items():
                        node[p[0]] = p[1]
                    node["isCompound"] = nodeIsCompound(node)
                    node["doublets"] = getNumberOfDoublets(node)
                    node["parents"] = getNumberOfParents(node)
                    nodes.append(node)

                cachedTerm = {}
                cachedTerm["nodes"] = nodes
                cachedTerm["searchTerm"] = searchTerm
                cachedTerm["languageCode"] = languageCode
                cachedTerm["count"] = 0
                cachedTerm["lastDate"] = datetime.now()
                collection.insert(cachedTerm)
                if len(cachedTerm["nodes"]) > 0:
                    roots.append(cachedTerm)


if __name__ == "__main__":
    refreshCache()
