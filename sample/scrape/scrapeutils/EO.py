import re
from . import Classes
from . import Misc
from . import LanguageConverters


printDivider = Misc.printDivider
Word = Classes.Word
Link = Classes.Link
getLanguageName = LanguageConverters.getLanguageName
getLanguageCode = LanguageConverters.getLanguageCode

testMode = 1

linkTypes = {
    "der": "derived",
    "bor": "borrowed",
    "m": "mention",
    "suf": "suffix",
    "inh": "inherited",
    "lbor": "learned borrowing",
    "obor": "orthographic borrowing",
    "psm": "phono-semantic matching",
    "com": "compound",
    "pre": "prefix",
    "af": "affix",
    "con": "confix",
    "cal": "calque",
}


def getFullLinkTypeName(shortForm):
    return linkTypes[shortForm]


def appendDash(inputWord):
    index = inputWord.find("-")
    if index == len(inputWord)-1:
        return inputWord
    else:
        return inputWord + "-"


def prependDash(inputWord):
    index = inputWord.find("-")
    if index == 0:
        return inputWord
    else:
        return "-" + inputWord


def processEtymologyObject(eo, lastWord, articleTitle, isCompound=False):
    linkType = eo[0]
    if linkType in linkTypes:
        linkType = getFullLinkTypeName(linkType)

    def getTransliteratedForm(eo):
        try:
            transliteratedForm = list(
                filter(lambda e: re.match(r'^tr=(.+)', e), eo))[0][3:]
            return transliteratedForm
        except:
            return None

    nonEtymologicalLinks = ["m", "mention"]

    # meeting first IF condition implies there are >1 ancestors
    if linkType in ["compound", "com", "suf", "suffix", "pre", "prefix", "con", "confix"]:
        languageCode = eo[1]

        # are there transliterations?
        try:
            latinTranscription1 = list(
                filter(lambda e: re.match(r'^tr1=(.+)', e), eo))[0][4:]
        except IndexError:
            try:
                latinTranscription1 = eo[2]
            except IndexError:
                latinTranscription1 = ""
                pass
            pass

        try:
            latinTranscription2 = list(
                filter(lambda e: re.match(r'^tr2=(.+)', e), eo))[0][4:]
        except IndexError:
            try:
                latinTranscription2 = eo[3]
            except IndexError:
                latinTranscription2 = ""
                pass
            pass

        if latinTranscription1 not in ["-", ""]:
            if linkType in ["prefix", "pre", "confix", "con"]:
                latinTranscription1 = appendDash(latinTranscription1)
            word1 = Word(latinTranscription1, {'name': getLanguageName(
                languageCode), 'code': languageCode})
            Link(word1, lastWord, linkType, articleTitle)

        if latinTranscription2 not in ["-", ""]:
            if linkType in ["suffix", "suf"] or (linkType in ["confix", "con"] and len(eo) < 5):
                latinTranscription2 = prependDash(latinTranscription2)
            word2 = Word(latinTranscription2, {'name': getLanguageName(
                languageCode), 'code': languageCode})
            Link(word2, lastWord, linkType, articleTitle)

        # in case of confixes, we could have 3 words coming together
        if (linkType == "confix" or linkType == "con") and len(eo) >= 5 and eo[4].find("=") == -1:
            try:
                latinTranscription3 = list(
                    filter(lambda e: re.match(r'^tr3=(.+)', e), eo))[0][4:]
            except IndexError:
                latinTranscription3 = eo[4]
                pass
            if latinTranscription3 not in ["-", ""]:
                latinTranscription3 = prependDash(latinTranscription3)
                word3 = Word(latinTranscription3, {'name': getLanguageName(
                    languageCode), 'code': languageCode})
                Link(word3, lastWord, linkType, articleTitle)

        return lastWord
    elif linkType in ["blend", "affix", "af"]:
        # if the link is a blend we have an unknown number of ancestors
        languageCode = eo[1]

        # first 2 elements are linkType and language
        # filter list to return only those with no = in them
        blendComponents = list(
            filter(lambda e: e.find("=") == -1 and e != "", eo[2:]))
        i = 0

        # for each of our component wordparts
        for component in blendComponents:
            i = i + 1
            # are there transliterations?
            trMatch = re.compile('^tr' + str(i) + '=(.+)')
            try:
                latinTranscription = list(
                    filter(lambda e: trMatch.match(e), eo))[0][4:]
            except IndexError:
                latinTranscription = component
                pass

            if latinTranscription in ["-", ""]:
                return None
            word = Word(latinTranscription, {'name': getLanguageName(
                languageCode), 'code': languageCode})
            Link(word, lastWord, linkType, articleTitle)

    elif linkType == "clipping":
        languageCode = eo[1]

        # try and get transliteration
        try:
            latinTranscription = list(
                filter(lambda e: re.match(r'^tr=(.+)', e), eo))[0][3:]
        except IndexError:
            latinTranscription = eo[2]
            pass

        if latinTranscription in ["-", ""]:
            return None
        word = Word(latinTranscription, {'name': getLanguageName(
            languageCode), 'code': languageCode})

        Link(word, lastWord, linkType, articleTitle)
        return word

    else:  # else one ancestor

        # in this if block we set the ancestor language code
        # and latin transcription
        if linkType in ["inh", "inherited", "der", "derived", "bor", "borrowed", "lbor", "learned borrowing", "obor", "orthographic borrowing", "cal", "calque", "semantic loan", "phono-semantic matching", "psm"]:
            try:
                transliteratedForm = getTransliteratedForm(eo)
                ancestorLatinTranscription = eo[3] if transliteratedForm is None else transliteratedForm
                ancestorLanguageCode = eo[2]
            except IndexError:  # sometimes we don't have enough
                return None

        elif linkType in ['m', 'mention', "short for", "back-form"]:
            ancestorLanguageCode = eo[1]
            transliteratedForm = getTransliteratedForm(eo)
            try:
                ancestorLatinTranscription = eo[2] if transliteratedForm is None else transliteratedForm
            except IndexError:
                return None

        elif linkType == "named-after":
            languageCode = eo[1]
            transliteratedForm = getTransliteratedForm(eo)
            if transliteratedForm:
                latinTranscription = transliteratedForm
            else:
                try:
                    latinTranscription = list(
                        filter(lambda e: e.find("=") == -1, eo[2:]))[0]
                except IndexError:
                    return None

            if latinTranscription in ["-", ""]:
                return None
            word = Word(latinTranscription, {'name': getLanguageName(
                languageCode), 'code': languageCode})

            Link(word, lastWord, linkType, articleTitle)

            return word

        else:
            # unknown link (probably not etymological), so return
            # without adding
            return None

        # Wiktionary sometimes returns "-" as the word when
        # the actual ancestor is unknown
        if ancestorLatinTranscription in ["-", ""]:
            return None

        word = Word(ancestorLatinTranscription, {'name': getLanguageName(
            ancestorLanguageCode), 'code': ancestorLanguageCode})

        if isCompound:
            linkType = "compound"
        Link(word, lastWord, linkType, articleTitle)

        # some of the link types aren't etymological (e.g. mentions of other words). if this is 
        # one of them then don't return the new word as we want to chain to continue from the
        # last word
        if linkType in nonEtymologicalLinks:
            return None
        else:
            return word
