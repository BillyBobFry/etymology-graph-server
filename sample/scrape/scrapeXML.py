import xml.etree.ElementTree as ET
import re
from neo4j import GraphDatabase
from pymongo import MongoClient
import lingpy as lp
import os
import sys
import time
from transliterate import translit, get_available_language_codes
from scrapeutils import LanguageConverters, Misc, EO, Classes, TransliterateDict


getLanguageName = LanguageConverters.getLanguageName
getLanguageCode = LanguageConverters.getLanguageCode
printDivider = Misc.printDivider
stripDodgyChars = Misc.stripDodgyChars
processEtymologyObject = EO.processEtymologyObject
Word = Classes.Word
Link = Classes.Link

# mongodb vars
client = MongoClient('localhost', 27017)
linguisticsDB = client.linguistics
allLanguages = linguisticsDB["languages"].find()

erroredLangs = []


# set neo4j vars
uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)
outfile = open("output.txt", "a+", encoding="utf-8")
outfile.write("Test")


def parseTree(wiktionaryPage):
    article = wiktionaryPage.find("revision").find("text").text
    articleTitle = wiktionaryPage.find("title").text

    if article is None:
        return

    # only parse entries without spaces (single words)
    if articleTitle.find(" ") != -1 or articleTitle.find("Wiktionary:") != -1 or articleTitle.find("Index:") != -1 or articleTitle.find("Glossary:") != -1 or articleTitle.find("Appendix:") != -1 or articleTitle.find("Rhymes:") != -1:
        return

    print(f"parsing {articleTitle}")
    # Will's interpretation of wiktionary pages:
    # an ARTICLE is the text of a page
    # a SECTION is a part of an article introduced by ==SECTION_NAME==
    # an ENTRY is a part of a section introduced by ===ENTRY_NAME===

    # make sure we start from the first entry
    startOfArticle = article.find("==")
    article = article[startOfArticle:]

    # split article by language
    entries = re.split(r'==([\w ]+)==\n', article)
    if entries[0] == "":
        entries.pop(0)

    # now even entries are language names, odd entries are articles
    while len(entries) > 1:
        leafLanguageName = entries.pop(0)
        leafLanguageCode = getLanguageCode(leafLanguageName)
        section = entries.pop(0)
        startOfSection = section.find("===")
        section = section[startOfSection:]

        sections = re.split(r'===([\w ]*?)===\n', section)
        if sections[0] == "":
            sections.pop(0)

        articleObject = {leafLanguageName: {}}

        while len(sections) > 1:
            sectionName = sections.pop(0).lower()
            sectionText = sections.pop(0).lower()
            articleObject[leafLanguageName][sectionName] = sectionText

        etymologyHeaderRE = re.compile(r'^(etymology( \d+)?)$')
        try:
            sectionNames = list(articleObject[leafLanguageName].keys())
            etymologyHeaderName = list(
                filter(lambda e: etymologyHeaderRE.match(e), sectionNames))[0]
            etymologyText = articleObject[leafLanguageName][etymologyHeaderName]
        except IndexError:
            # sometimes we have no etymology section
            # in those cases we don't give a crap about the word
            continue

        if testMode == 1:
            print("SECTION NAMES:")
            print(sectionNames)
        # can we find an IPA pronunciation?
        if "pronunciation" in sectionNames:
            pronunciationText = articleObject[leafLanguageName]["pronunciation"]
            if testMode == 1:
                print(pronunciationText)
            ipaSection = re.search(
                r'\{\{ipa\|.*?\|(.*?)\}\}', pronunciationText)
            try:
                ipaPronunciation = ipaSection.groups()[0].split("|")[0]
            except IndexError:
                ipaPronunciation = None
                pass
            except AttributeError:
                ipaPronunciation = None
                pass
        else:
            ipaPronunciation = None

        # etymology descripts are structured as:
        # "From English foobius, from Middle English barbius,
        #  from Old English doobius, from doo + bius"
        lastWord = Word(
            articleTitle, {'code': leafLanguageCode, 'name': leafLanguageName}, ipaPronunciation)

        # sometimes there are etymology sections with nothing in them
        if etymologyText.find("{{") == -1:
            continue

        # remove anything inside () brackets (usually included in etymology sections as asides)
        openBracket = etymologyText.find("(")
        closeBracket = etymologyText.find(")")
        while openBracket != -1 and closeBracket != -1:

            # if there is a close bracket before the next
            # open bracket, remove the close bracket
            if closeBracket < openBracket:
                etymologyText = etymologyText[:closeBracket] + \
                    etymologyText[closeBracket + 1:]
                openBracket = etymologyText.find("(")
                closeBracket = etymologyText.find(")")
                continue

            # check if we have nested brackets
            nextBracket = etymologyText.find("(", openBracket+1)
            while nextBracket < closeBracket and nextBracket != -1:
                openBracket = nextBracket
                nextBracket = etymologyText.find("(", openBracket + 1)

            etymologyText = etymologyText[:openBracket] + \
                etymologyText[closeBracket+1:]
            openBracket = etymologyText.find("(")
            closeBracket = etymologyText.find(")")

        t = re.compile(
            r'(?:<ety>([\w\s\d\(\)\+,]*(\{\{.*?\}\})[\w\s\d\(\)\+,]*)*)')
        t = re.compile(
            r'(?:<ety>([^\.]*(\{\{.*?\}\})[^\.]*)*)')
        etMatch = t.match(etymologyText)

        if etMatch:
            etymologyText = etMatch.groupdict()["ety"]

        stepMatch = re.compile(
            r'(\s?(?:cognate (?:to )?|borrowed |loaned )?(?:from )?\{\{(?:.*?)\}\}(?:[^\.,;\{\}]+?\{\{(?:.+?)\}\})*[^\.,;\{\}]*)')
        stepMatch = re.compile(
            r'(?:\w+ |^)(?<!cognate to )\{\{(?:.*?)\}\}(?:[^\.,;\{\}]+?\{\{(?:.+?)\}\})*[^\.,;\{\}]*')
        allSteps = stepMatch.findall(etymologyText)

        if testMode:
            print("----------------------------")
            print("Etymology text to parse:")
            print("----------------------------")
            print(etymologyText)

            print("----------------------------")
            print("Steps:")
            print(allSteps)
            # print(allSteps.groups())
            print("----------------------------")

        # ASSUMPTION: each "step" of the etymology chain is separated by
        # the string ", from" (got to use something, right?)
        for step in allSteps:

            if step.find("{{") == -1 or step.find("}}") == -1 or step[:8] == " cognate":
                # sometimes the step is a sham
                continue

            if testMode == 1:
                print("------------step------------")
                print(step)

            compoundPattern = re.compile(
                r'\{\{((?!etyl).*?)\}\}.*?\+.*?\{\{((?!etyl).*?)\}\}')
            linkIsCompound = compoundPattern.search(step)
            if testMode == 1:
                printDivider()
                print("step is compound?")
                print(linkIsCompound)
                if linkIsCompound:
                    print(linkIsCompound.groups())
                printDivider()

            if linkIsCompound:
                components = linkIsCompound.groups()
                eo1 = components[0]
                eo2 = components[1]
                processEtymologyObject(
                    eo1.split("|"), lastWord, articleTitle, isCompound=True)
                processEtymologyObject(
                    eo2.split("|"), lastWord, articleTitle, isCompound=True)
                break
            else:
                etymologycalObjectPattern = re.compile(r'\{\{(.*?)\}\}')
                etymologicalObjects = etymologycalObjectPattern.findall(step)
                if testMode == 1:
                    printDivider()
                    print("Found the following etymological objects:")
                    print(etymologicalObjects)
                    printDivider()
                for eo in etymologicalObjects:
                    try:
                        newWord = processEtymologyObject(
                            eo.split("|"), lastWord, articleTitle)
                        if newWord:  # only update the last word if we were able to find one
                            lastWord = newWord
                            break
                    except:
                        print("-----------------------")
                        print("ERROR")
                        print(eo)
                        print("-----------------------")
                        erroredPages.append(articleTitle)
                        continue


def parseDirectory(directory, limit=None):
    i = 0
    allFiles = os.listdir(directory)
    allFiles.sort(key=lambda x: int(x[:x.find(".")]))
    for fileName in allFiles:
        f = open(path + '/' + fileName, "r", encoding="utf8")
        print("opened " + fileName)
        fileContents = f.read()

        xTree = ET.fromstring("<r>" + fileContents + "</r>")
        pages = xTree.findall("page")
        for page in pages:
            i = i + 1
            print("parsing page " + str(i))
            parseTree(page)
            if i == limit:
                return


path = '../../raw'
erroredPages = []
if len(sys.argv) == 1:
    testMode = False
    print(path)
    parseDirectory(path)
else:
    testMode = False
    # if we pass in a number, parse that amount of pages
    try:
        limit = int(sys.argv[1])
        parseDirectory(path, limit)
    except ValueError:
        testMode = True
        # if we pass in a string, parse that file
        page = ET.parse('testfiles/' + sys.argv[1] + '.xml')
        parseTree(page)


print(erroredPages)
print(erroredLangs)
# bash command to split

# make 500 directories
# $ for i in {1..3..1};
# do
#   csplit -ksf part. remaining.xml /\<page\>/ "{10000}" 2>/dev/null
#   mv -f part.2001 remaining.xml
#   cat part.* > $i.xml
# done

#  for i in {1..2000..1}; 
#    do csplit -ksf part. remaining.xml /\<page\>/ "{10000}" 2>/dev/null; 
#    mv -f part.10001 remaining.xml; 
#    cat part.* > $i.xml;
#  done
#


 for i in {1..2000..1}; 
   do cat $i.xml | grep '<title>cyclically' && echo $i
 done
#

# regex for etymology line
# From ([\w\s\d,]*(\{\{.*?\}\}))*
# or
# (from |(\{\{.*?\}\}))([\w\s\d,]*(\{\{.*?\}\})[\w\s\d,]*)*
# r'(?P<ety>(from |(\{\{.*?\}\}))([\w\s\d,]*(\{\{.*?\}\})[\w\s\d,]*)*)')

# cat ../output.log | grep -oP '(?!opened )\d+\.xml' | xargs mv -t ../done
