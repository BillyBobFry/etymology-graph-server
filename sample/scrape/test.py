from lang_trans.arabic import iso233, buckwalter
from polyglot import text
from polyglot.downloader import downloader
from transliterate import translit, get_available_language_codes, discover
discover.autodiscover()


def transliterateWord(word, originalLang):
    print(
        f"word {word} transliterates to {translit(word, originalLang, reversed=True)}")


greekWord = "πρᾶγμα"
transliterateWord(greekWord, 'grc')

greekWord = "πρῆγμᾰ"
transliterateWord(greekWord, 'grc')

greekWord = "πρῆχμᾰ"
transliterateWord(greekWord, 'grc')

greekWord = "πράγματος"
transliterateWord(greekWord, 'grc')

greekWord = "pragmatos"
transliterateWord(greekWord, 'grc')
print("arabic")
arabicWord = u"َوَالَة"
transliterateWord(arabicWord, 'ar')

arabicWord = u"الدِّسْتُور"
transliterateWord(arabicWord, 'ar')

arabicWord = u"َأَبُو"
transliterateWord(arabicWord, 'ar')
# hindiword = "ग्रह"
# transliterateWord(hindiword, 'hi')

# print(downloader.supported_languages_table("transliteration2"))
# print(downloader.supported_languages_table("morph2"))

w = text.Word(u"َوَالَة", language='ar')
print(w.transliterate("en"))

w = text.Word(u"الدِّسْتُور", language='ar')
print(w.transliterate("en"))

print(buckwalter.transliterate("َوَالَة"))
print(buckwalter.transliterate("الدِّسْتُور"))
print(buckwalter.transliterate("أَبُو"))
print(iso233.transliterate("َوَالَة"))
print(iso233.transliterate("الدِّسْتُور"))
print(iso233.transliterate("أَبُو"))
w = text.Word("ग्रह", language='hi')
print(w.transliterate("en"))

w = text.Word("ग्रह्", language='hi')
print(w.transliterate("en"))

w = text.Word("𑀅𑀧𑁆𑀧𑀸", language='hi')
print(w.transliterate("en"))

w = text.Word("ग्रह्", language='hi')
print(w.transliterate("en"))
