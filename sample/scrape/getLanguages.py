# This script scrapes the list of languages from wiktionary, and inserts code/name combinations into mongodb

import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
linguistics_db = client.linguistics
languages_collection = linguistics_db["languages"]
languages_collection.drop()
LANGUAGES_URL = 'https://en.wiktionary.org/wiki/Wiktionary:List_of_languages'

languages_response = requests.get(LANGUAGES_URL)

languages_body = BeautifulSoup(languages_response.content, 'html.parser')

language_tables = languages_body.find_all('table')
def getCodeAndNameFromRow(row):
    """Returns a language's code and name from its beautifulsoup row"""
    cells = row.find_all('td')
    # print(cells.stripped_strings)
    code, name, *others = list(cell.get_text(strip=True) for cell in cells)
    return [code, name]

def addLanguageToMongo(code, name):
    """Adds a language to mongodb"""
    languages_collection.insert({"code": code, "name": name})

def getLanguagesFromTable(table):
    language_rows = table.find('tbody').find_all('tr')

    # print(language_rows)
    for row in language_rows:
        try:
            code, name = getCodeAndNameFromRow(row)
            addLanguageToMongo(code, name)
            # print(code,name)
        except ValueError:
            pass

for table in language_tables:
    getLanguagesFromTable(table)