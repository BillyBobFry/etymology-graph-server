from neo4j import GraphDatabase
uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)


class Language:
    """
    A word in a language.
    ...

    Attributes
    ----------
    latinTranscription : string
        The word, represented in the Latin alphabet
    language : object {'name': string, 'code': string}
        The language of the word
    """

    def __init__(self, languageCode, startYear, endYear=9999):
        self.languageCode = languageCode
        if startYear > endYear:
            raise "Start year is greater than end year"
        self.startYear = startYear
        self.endYear = endYear

    def get_valid_descendants(self, allLanguages):
        return filter(lambda l: l.endYear > self.startYear, allLanguages)

    def get_invalid_descendants(self, allLanguages):
        return filter(lambda l: l.endYear < self.startYear, allLanguages)


languages = []

# PIE = 'ine-pro'
PIE = Language("ine-pro", -5000, -2000)
languages.append(PIE)

PROTO_ITALIC = Language('itc-pro', -2500, -700)
languages.append(PROTO_ITALIC)

PROTO_HELLENIC = Language('grk-pro', -3200, -1700)
languages.append(PROTO_HELLENIC)

MYCENAEAN_GREEK = Language('gmy', -1699, -1200)
languages.append(MYCENAEAN_GREEK)

ANCIENT_GREEK = Language('grc', -900, 400)
languages.append(ANCIENT_GREEK)

PROTO_BALTO_SLAVIC = Language('ine-bsl-pro', -1999, -1000)
languages.append(PROTO_BALTO_SLAVIC)

PROTO_SLAVIC = Language('sla-pro', -999, 600)
languages.append(PROTO_SLAVIC)

PROTO_BALTIC = Language('bat-pro', -999, 600)
languages.append(PROTO_BALTIC)

LITHUANIAN = Language('lt', 1500)
languages.append(LITHUANIAN)

LATVIAN = Language('lv', 1544)
languages.append(LATVIAN)

PROTO_GERMANIC = Language("gem-pro", -2500, 300)
languages.append(PROTO_GERMANIC)

LATIN = Language("la", -200, 900)
languages.append(LATIN)

VULGAR_LATIN = Language("vl.", -200, 449)
languages.append(LATIN)

LATE_LATIN = Language("ll.", 200, 500)
languages.append(LATE_LATIN)

MEDIEVAL_LATIN = Language("ml.", 501, 1350)
languages.append(MEDIEVAL_LATIN)

PROTO_WEST_GERMANIC = Language('gmw-pro',301, 449)
languages.append(PROTO_WEST_GERMANIC)

OLD_HIGH_GERMAN = Language("goh", 750, 1050)
languages.append(OLD_HIGH_GERMAN)

MIDDLE_HIGH_GERMAN = Language("gmh", 1051, 1350)
languages.append(MIDDLE_HIGH_GERMAN)

GERMAN = Language("de", 1351)
languages.append(GERMAN)

OLD_FRENCH = Language("fro", 700, 1350)
languages.append(OLD_FRENCH)

MIDDLE_FRENCH = Language("frm", 1351, 1650)
languages.append(MIDDLE_FRENCH)

FRENCH = Language("fr", 1651)
languages.append(FRENCH)

OLD_SPANISH = Language("osp", 1000, 1500)
languages.append(OLD_SPANISH)

SPANISH = Language("es", 1501)
languages.append(SPANISH)

OLD_ITALIAN = Language("it-oit", 960, 1581)
languages.append(OLD_ITALIAN)

ITALIAN = Language("it", 1582)
languages.append(ITALIAN)

OLD_NORTHERN_FRENCH = Language("onf.", 700, 1350)
languages.append(OLD_NORTHERN_FRENCH)

PROTO_NORSE = Language('gmq-pro', 301, 799)
languages.append(PROTO_NORSE)

OLD_NORSE = Language('non', 800, 1500)
languages.append(OLD_NORSE)

DANISH = Language("da", 1501)
languages.append(DANISH)

SWEDISH = Language("sv", 1501)
languages.append(SWEDISH)

NORWEIGAN = Language("no", 1501)
languages.append(NORWEIGAN)

NORWEIGAN_NYNORSK = Language("nn", 1501)
languages.append(NORWEIGAN_NYNORSK)

NORWEIGAN_BOKMAL = Language("nb", 1501)
languages.append(NORWEIGAN_BOKMAL)

OLD_ENGLISH = Language("ang", 450, 1066)
languages.append(OLD_ENGLISH)

MIDDLE_ENGLISH = Language("enm", 1067, 1500)
languages.append(MIDDLE_ENGLISH)

ENGLISH = Language("en", 1501)
languages.append(ENGLISH)

OLD_FRISIAN = Language("ofs", 900, 1699)
languages.append(OLD_FRISIAN)

WEST_FRISIAN = Language("fy", 900, 1699)
languages.append(WEST_FRISIAN)

OLD_DUTCH = Language("odt", 400, 1150)
languages.append(OLD_DUTCH)

MIDDLE_DUTCH = Language("dum", 1151, 1500)
languages.append(MIDDLE_DUTCH)

DUTCH = Language("nl", 1501)
languages.append(DUTCH)

PROTO_CELTIC = Language("cel-pro", -800, 0)
languages.append(PROTO_CELTIC)

OLD_IRISH = Language("sga", 600, 900)
languages.append(OLD_IRISH)

PROTO_ALBANIAN = Language("sqj-pro", -167, 600)
languages.append(PROTO_ALBANIAN)

ALBANIAN = Language("sq", 601)
languages.append(ALBANIAN)

PROTO_ARMENIAN = Language("hyx-pro", -1200, 500)
languages.append(PROTO_ALBANIAN)

OLD_ARMENIAN = Language("xcl", 501, 1800)
languages.append(PROTO_ALBANIAN)

ARMENIAN = Language("hy", 1801)
languages.append(ALBANIAN)


t0 = [
    PIE
]
t10 = [PROTO_ITALIC, PROTO_BALTO_SLAVIC]
t20 = [PROTO_GERMANIC, LATIN]
t25 = [PROTO_WEST_GERMANIC, PROTO_NORSE]
t30 = [OLD_ENGLISH, OLD_FRENCH, OLD_NORSE]
t40 = [MIDDLE_ENGLISH, MIDDLE_FRENCH]
t50 = [ENGLISH, FRENCH]

orderedTiers = [t0, t10, t20, t25, t30, t40, t50]

queryText = f"""match (a:word)-[r]->(b:word) 
            where b.languageCode = $older
            and a.languageCode = $younger
            delete r"""

invalidDescendantsOfEnglish = ENGLISH.get_invalid_descendants(languages)
for vd in invalidDescendantsOfEnglish:
    print(vd.languageCode)

for lang in languages:

    invalidDescendants = lang.get_invalid_descendants(languages)
    for invalidDescendant in invalidDescendants:
        bindVars = {
            "older": invalidDescendant.languageCode,
            "younger": lang.languageCode,
        }
        with driver.session() as session:
            print(f"Removing invalid {invalidDescendant.languageCode}-{lang.languageCode} links.")
            session.run(queryText, **bindVars)


# for i in range(len(orderedTiers)):
#     thisTier = orderedTiers[i]
#     forbiddenDescendants = []
#     forbiddenTiers = orderedTiers[i + 1:]
#     for tier in forbiddenTiers:
#         forbiddenDescendants = forbiddenDescendants + tier

#     print("This tier")
#     print(orderedTiers[i])
#     print("VERBOTEN")
#     print(forbiddenDescendants)

#     for oldLanguage in thisTier:
#         for youngLanguage in forbiddenDescendants:
#             bindVars = {
#                 "older": oldLanguage,
#                 "younger": youngLanguage,
#             }
#             with driver.session() as session:
#                 session.run(queryText, **bindVars)

#             print(oldLanguage)
#             print(youngLanguage)
