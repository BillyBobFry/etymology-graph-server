from neo4j import GraphDatabase
from . import Misc
from transliterate import translit
from . import TransliterateDict
import polyglot
from polyglot.transliteration import Transliterator
from polyglot.text import Word as PGWord
import lingpy as lp

testMode = 1

# transliterateDict = TransliterateDict.transliterateDict
stripDodgyChars = Misc.stripDodgyChars
printDivider = Misc.printDivider

# set neo4j vars
uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)
outfile = open("output.txt", "a+", encoding="utf-8")
outfile.write("Test")


def formatRelationshipType(type):
    return type.replace("-", "_").replace(" ", "_")


class Word:
    """
    A word in a language.
    ...

    Attributes
    ----------
    latinTranscription : string
        The word, represented in the Latin alphabet
    language : object {'name': string, 'code': string}
        The language of the word
    """

    def __init__(self, word, language, ipa=None):

        # transliterate if necessary
        if language["code"] in TransliterateDict.polyglotLanguages:
            latinTranscription = PGWord(
                word, language=language["code"]).transliterate("en")
        elif language["code"] in TransliterateDict.translitLanguages:
            latinTranscription = translit(
                word, language["code"], reversed=True)
        elif language["code"] in TransliterateDict.sharedScripts:
            sharedLang = TransliterateDict.sharedScripts[language["code"]]
            latinTranscription = PGWord(
                word, language=sharedLang).transliterate("en")
        else:
            # else word is probably already in latin alphabet
            latinTranscription = word

        latinTranscription = stripDodgyChars(latinTranscription)

        self.latinTranscription = latinTranscription
        self.language = language
        self.ipa = ipa

        if latinTranscription == "" or latinTranscription == "-":
            return None
        else:
            # insert the word into neo4j
            with driver.session() as session:
                try:
                    existingWord = session.run(f'match (a:word) '
                                        f' where a.languageCode = "{language["code"]}"'
                                        f' and a.latinTranscription = "{latinTranscription}"'
                                        f' return a')

                    wordExists = False if existingWord.peek() is None else True
                    if wordExists:

                        # see if it needs to be given an IPA transcription
                        if existingWord.peek()[0]["ipa"] == "" and ipa:
                            existingWord = session.run("""match (a:word) 
                                                        where a.languageCode = $languageCode
                                                        and a.latinTranscription = $latinTranscription
                                                        set a.ipa=$ipa
                                                        return a """,
                                                    latinTranscription=latinTranscription,
                                                    languageCode=language["code"],
                                                    ipa=ipa
                                                    )
                    else:  # create the word
                        dbIPA = ipa or ""
                        session.run(
                            'CREATE (a:word { latinTranscription: $latinTranscription, originalForm: $word, ipa: $ipa, languageCode: $languageCode, languageName: $languageName }) return a',
                            latinTranscription=latinTranscription,
                            word=word,
                            languageCode=language["code"],
                            languageName=language["name"],
                            ipa=dbIPA
                        )
                except:
                    # sometimes wiktionary has weird "words" that break our syntax
                    return None


class Link:
    """
    An etymological link between 2 words.
    ...

    Attributes
    ----------
    parent : Word
        An object of class word
    child : Word
        An object of class word
    relationshipType: string
        How the parent and child are related
    """

    def __init__(self, parent, child, relationshipType, articleTitle):
        self.parent = parent
        self.child = child

        # neo4j doesn't accept relationship names with a "-"
        relationshipType = relationshipType.replace("-", "_")
        self.relationshipType = relationshipType

        # get sound progressions
        try:
            sequence = [parent.latinTranscription.replace(
                "*", ""), child.latinTranscription.replace("*", "")]

            msa = lp.Multiple(sequence)
            msa.prog_align()
            progressions = []
            lengthOfComp = len(parent.latinTranscription) + \
                len(child.latinTranscription)

            for i in range(lengthOfComp):
                progressions.append([])

            for i in range(len(msa.alm_matrix)):
                word = msa.alm_matrix[i]
                for j in range(len(word)):
                    progressions[j].append(word[j])

            # progressions is now a list of the form:
            # [[h,h],[e,e],[-,b],[ll,l],[o,a]]
            # (example given for hello->hebla)
            progressions = list(filter(lambda e: len(e) > 0, progressions))
            # format progressions into string of form:
            # h>h,e>e,->b,ll>l,o>a
            progressionString = ','.join(list(
                map(lambda e: '>'.join(e), progressions)))
        except ValueError:
            # lingpy throws an error if all characters are unknown
            progressionString = ""
            pass
        except IndexError:
            progressionString = ""
            pass

        with driver.session() as session:
            existingLink = session.run(f'match path = (a)-[r:{formatRelationshipType(relationshipType)}]->(b)'
                                       ' where a.languageCode = $parentLanguageCode'
                                       ' and a.latinTranscription = $parentLatinTranscription'
                                       ' and b.languageCode = $childLanguageCode'
                                       ' and b.latinTranscription = $childLatinTranscription'
                                       ' and any(rel in relationships(path) where rel.articleTitle = $articleTitle) '
                                       f" return relationships(path)",
                                       parentLatinTranscription=parent.latinTranscription,
                                       parentLanguageCode=parent.language["code"],
                                       childLatinTranscription=child.latinTranscription,
                                       childLanguageCode=child.language["code"],
                                       relationshipType=relationshipType,
                                       articleTitle=articleTitle
                                       )
            linkExists = False if existingLink.peek() is None else True
            if linkExists == False:
                session.run('match (a:word),(b:word)'
                            ' where a.languageCode = $parentLanguageCode'
                            ' and a.latinTranscription = $parentLatinTranscription'
                            ' and b.languageCode = $childLanguageCode'
                            ' and b.latinTranscription = $childLatinTranscription'
                            f' create (a)-[r:{formatRelationshipType(relationshipType)}'
                            ' {progressions: $progressions, articleTitle: $articleTitle}]->(b)'
                            ' return r',
                            parentLatinTranscription=parent.latinTranscription,
                            parentLanguageCode=parent.language["code"],
                            childLatinTranscription=child.latinTranscription,
                            childLanguageCode=child.language["code"],
                            relationshipType=relationshipType,
                            progressions=progressionString,
                            articleTitle=articleTitle
                            )
            else:
                if testMode == 1:
                    print("link exists")
