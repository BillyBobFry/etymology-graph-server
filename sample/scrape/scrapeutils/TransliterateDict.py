polyglotLanguages = [
    "hi",  # Hindi
    "ar",  # Arabic
    "nn",  # Norweigan Nynorsk
    "ka",  # Georgian
    "yi",  # Yiddish
    "ja",  # Japanese
    "el",  # Greek
    "vi",  # Vietnamese
    "mk",  # Macedonian
    "bs",  # Bosnian
    "hr",  # Croatian
    "sr",  # Serbian
    "az",  # Azerbaijani
    "bg",  # Bulgarian
    "no",  # Norweigan
    "et",  # Estonian
    "be",  # Belarusian
    "mr",  # Marathi
    "hy",  # Armenian
    "sl",  # Slovenian
    "ko",  # Korean
    "bn",  # Bengali
    "ga",  # Irish
    "tl",  # Tagalog
    "sw",  # Swahili
    "uk",  # Ukrainian
    "ur",  # Urdu
    "gu",  # Gujarati
    "ta",  # Tamil
    "km",  # Khmer
    "ms",  # Malay
    "id",  # Indonesian
    "he",  # Hebrewlith
    "lt",  # Lithuanian
    "tr",  # Turkish
    "hu",  # Hungarian
    "th",  # Thai
    "ru",  # Russian

]

translitLanguages = [
    'hy',  # Armenian
    'ka',  # Georgian
    'el',  # Greek
    'ru',  # Russian
    'grc',  # Ancient Greek
]

sharedScripts = {
    "sa": "hi"  # Sanksrit/Hindi
}
