from flask import Flask, jsonify,abort, request
from neo4j import GraphDatabase
from pymongo import MongoClient
from flask_cors import CORS
import json
# from neo4jUtils import runCypherQuery, runCypherQueryWithDict, autocomplete, getChildrenOfTwoNodes, getDoubletsForLanguage, getDoubletsForWord, getGrassmanLinks, getIgvaeonicLinks, getIgvaeonicParents, getLinks, getLinksWithSingleStep, getNodesFromBoltStatementResult
import datetime
from flask_caching import Cache
from dotenv import load_dotenv
import os

load_dotenv()


def runCypherQuery(queryText, bindVars):
    with driver.session() as session:
        results = session.run(queryText, **bindVars)
        return results

def runCypherQueryWithDict(queryText, bindVars):
    with driver.session() as session:
        results = session.run(queryText, bindVars)
        return results

def serialiseNode(n):
    node = {}
    node["id"] = n.id
    for p in n.items():
        node[p[0]] = p[1]
    return node

def serialiseLink(r):
    relationship = {}
    relationship["source"] = r.start_node.id
    relationship["target"] = r.end_node.id
    relationship["ipa"] = r.get("ipa")
    relationship["soundProgressions"] = r.get("progressions")
    relationship["type"] = r.type
    
    return relationship

def getNodesFromBoltStatementResult(BSR):
    return getGraphFromBoltStatementResult(BSR)

def getGraphFromBoltStatementResult(BSR):
    g = BSR.graph()
    return getGraphFromBoltGraph(g)


def getGraphFromBoltGraph(g):
    nodes = []
    for n in g.nodes:
        node = {}
        node["id"] = n.id
        for p in n.items():
            node[p[0]] = p[1]
        nodes.append(node)

    relationships = []
    for r in g.relationships:
        relationship = {}
        nodesInRelationship = list(map(lambda e: e.id, r.nodes))
        relationship["source"] = r.start_node.id
        relationship["target"] = r.end_node.id
        relationship["ipa"] = r.get("ipa")
        relationship["soundProgressions"] = r.get("progressions")
        relationship["type"] = r.type
        relationships.append(relationship)

    return {"nodes": nodes, "links": relationships}

# --------------------

app = Flask(__name__)
CACHE_TIMEOUT = 31556952
cache_config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "RedisCache",  # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": CACHE_TIMEOUT,
    "CACHE_REDIS_URL": os.getenv(REDIS_URL, "redis://localhost:6379")
}
#
app.config.from_mapping(cache_config)
cache = Cache(app, cache_config)
CORS(app)

client = MongoClient('localhost', 27017)
linguisticsDB = client.linguistics

MongoClient = MongoClient('localhost', 27017)
cacheDB = MongoClient.cache
freetextCache = cacheDB["etymologyWords"]

uri = "bolt://localhost:7687"
wordCount=0
languageCount=0

try:
    driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)
    with driver.session() as session:
        countWordsQueryText = "match (a) with count(a) as count return count"
        wordCountResult = session.run(countWordsQueryText)
        record = wordCountResult.single()
        wordCount = record["count"]

        countLanguagesQueryText = "match (a) return distinct a.languageCode"    
        languageCountResult = session.run(countLanguagesQueryText, {})
        languageCount = len(languageCountResult.values())

except Exception as e:  # if neo4j isn't contactible we still want to run the rest of the site
    print(e)
    wordCount=1
    languageCount=1
    pass

@app.after_request
def apply_caching(response):
    response.headers["Cache-Control"] = "max-age=3600"
    return response


""" Return all languages """
@app.route("/languages")
def getAllLanguages():
    allLanguages = linguisticsDB["languages"].find()
    allLanguages = list(map(lambda e: [e["code"], e["name"]], allLanguages))
    return jsonify(allLanguages)

@app.route("/metadata")
def getMetadata():
    return jsonify({"words": wordCount, "languages": languageCount})

""" Find the language with the matching code in the database """
@app.route('/languages/<code>')
def getLanguageByCode(code):
    matchingLanguage = linguisticsDB["languages"].find_one({"code": code}, {"_id": 0})

    return matchingLanguage if matchingLanguage else abort(404)

""" """
@app.route('/doublets/language/<languageCode>/word/<word>')
@cache.cached(timeout=CACHE_TIMEOUT)
def handleGetDoublets(languageCode, word):
    nodes = getDoubletsForWord(languageCode, word)
    return jsonify(nodes)

"""  """ 
@app.route('/doublets/language/<languageCode>/search/<searchTerm>')
def handleDoubletsSearch(languageCode,searchTerm):
    matchingWords = autocomplete(languageCode, searchTerm)["nodes"]
    withDoublets = filter(lambda e: e["doublets"] > 0, matchingWords)
    return jsonify(list(withDoublets))

""" Search for words """
@app.route('/autocomplete/language/<languageCode>/search/<searchTerm>')
def handleAutocompleteSearch(languageCode, searchTerm):
    maximumWordLength = request.args.get("maximumWordLength")
    resp = autocomplete(languageCode, searchTerm, maximumWordLength)
    matchingWords = resp["nodes"]
    withAncestors = filter(lambda e: 'parents' not in e or e["parents"] > 0, matchingWords)
    return jsonify(list(withAncestors))

@app.route('/etymology/language/<languageCode>/word/<word>')
@cache.cached(timeout=CACHE_TIMEOUT)
def getEtymology(languageCode, word):
    maxPathLength = request.args.get("maxPathLength")
    if maxPathLength is None or int(maxPathLength) > 20: 
        maxPathLength = 10

    bindVars = {
        "languageCode": languageCode,
        "latinTranscription": word,
        "maxPathLength": maxPathLength
    }
    queryText = f"""match p=(a:word)-[r:borrowed|inherited|derived|compound|affix|blend|suffix|prefix|calque|confix|clipping *1..{bindVars['maxPathLength']}]->(b:word)
            where b.languageCode = $languageCode
            and b.latinTranscription = $latinTranscription
            and all (rel in relationships(p) where toLower(rel.articleTitle) = $latinTranscription)
            return nodes(p) as nodes, relationships(p) as links"""

    with driver.session() as session:
        result = session.run(queryText, bindVars)   
        resp = getGraphFromBoltGraph(result.graph())

        compoundRoots = list(
            filter(lambda link: link["type"] in ["compound", "suffix", "prefix", "affix", "confix"], resp["links"]))

        # if we have a compound, get the etymologies of both compound roots
        if (len(compoundRoots) > 0):
            sourceIDs = map(lambda link: link["source"], compoundRoots)
            for wordID in sourceIDs:
                word = list(
                    filter(lambda node: node["id"] == wordID, resp["nodes"]))[0]
                bindVars = {
                    "languageCode": word['languageCode'],
                    "languageName": word['languageName'],
                    "latinTranscription": word['latinTranscription']
                }
                result = runCypherQuery(queryText, bindVars)
                extraGraph = getNodesFromBoltStatementResult(result)

                # remove original compound root from extra nodes
                extraGraph["nodes"] = list(
                    filter(lambda node: node["id"] != wordID, extraGraph["nodes"]))
                if len(extraGraph["nodes"]) > 0:
                    resp["nodes"] = resp["nodes"] + extraGraph["nodes"]
                if len(extraGraph["links"]) > 0:
                    resp["links"] += extraGraph["links"]

        # do any of the root nodes themselves have compound etymologies?
        allNodes = [n["id"] for n in resp["nodes"]]
        allTargets = [l["target"] for l in resp["links"]]
        allRoots = filter(lambda n: n not in list(allTargets), list(allNodes))
        for root in allRoots:
            rootQueryText = f"""match p=(a:word)<-[r:compound]-(b:word)<-[s *0..8]-(c:word)
                    where id(a)=$rootID return nodes(p), relationships(p)"""
            rootBindVars = {
                "rootID": root
            }
            result = runCypherQuery(rootQueryText, rootBindVars)
            extraGraph = getNodesFromBoltStatementResult(result)

            # remove original compound root from extra nodes
            extraGraph["nodes"] = list(
                filter(lambda node: node["id"] != root, extraGraph["nodes"]))

            allNodes = map(lambda n: n["id"], resp["nodes"])
            extraGraph["nodes"] = list(
                filter(lambda n: n["id"] not in allNodes, extraGraph["nodes"]))

            if len(extraGraph["nodes"]) > 0:
                resp["nodes"] += extraGraph["nodes"]
            if len(extraGraph["links"]) > 0:
                resp["links"] += extraGraph["links"]
        return jsonify(resp)        
  
@app.route("/paths/from/<fromLanguageCode>/to/<toLanguageCode>")
@cache.cached(timeout=CACHE_TIMEOUT)
def handleGetPaths(fromLanguageCode, toLanguageCode):
    page = request.args.get("page") or 1
    pageSize = request.args.get("pageSize") or 100

    chains = getLinks(fromLanguageCode, toLanguageCode, int(page), int(pageSize))
    return jsonify(chains)

@app.route("/sound-changes/from/<fromLanguageCode>/<fromSounds>/to/<toLanguageCode>/<toSounds>")
@cache.cached(timeout=CACHE_TIMEOUT)
def handleGetSoundChanges(fromLanguageCode, toLanguageCode, fromSounds, toSounds):
    fromSounds = fromSounds.split(",")
    toSounds = toSounds.split(",")
    page = request.args.get("page") or 1
    pageSize = request.args.get("pageSize") or 100
    matches = getLinksWithSingleStep(fromLanguageCode, toLanguageCode, int(page), int(pageSize))
    
    links = matches["links"]
    uniqueLinks = []
    uniqueSourceTargets = []
    for link in links:
        sourceTarget = [link["source"], link["target"]]
        if sourceTarget not in uniqueSourceTargets:
            uniqueSourceTargets.append(sourceTarget)
            uniqueLinks.append(link)

    def containsSoundChange(link):
        soundChanges = link["soundProgressions"].split(",")
        for soundChange in soundChanges:
            try:
                _from,_to = soundChange.split(">")
                if _from in fromSounds and _to in toSounds:
                    return True
            except ValueError:
                continue
            

        return False

    linksWithSoundChange = filter( containsSoundChange, uniqueLinks)

    def getNodesFromLink(link):
        source = link["source"]
        target = link["target"]
        return list(filter(lambda e: e["id"] in [source, target],matches["nodes"]))

    graphsWithSoundChange = map(lambda e: {'link': e, 'nodes': getNodesFromLink(e)},linksWithSoundChange)
    return jsonify(list(graphsWithSoundChange))

@app.route("/sound-changes-expanded/from/<parent>/to/<child>")
@cache.cached(timeout=CACHE_TIMEOUT)
def handleExpandSoundChanges(parent, child):
    fromDescendant = request.args.get("fromDescendant")
    toDescendant = request.args.get("toDescendant")
    graph = getChildrenOfTwoNodes(parent, child, fromDescendant, toDescendant)

    nodes = graph["nodes"]
    uniqueNodes = []
    uniqueNodeIDs = []
    for node in nodes:
        nodeId = node["id"]
        if nodeId not in uniqueNodeIDs:
            uniqueNodeIDs.append(nodeId)
            uniqueNodes.append(node)

    return jsonify({'nodes': uniqueNodes, 'links': graph['links']})

@app.route("/grassmans-law/from/<parent>/to/<child>")
@cache.cached(timeout=CACHE_TIMEOUT)
def handleGrassmansLaw(parent, child):
    grassmanLinks = getGrassmanLinks(parent, child)

    return jsonify(grassmanLinks)

@app.route("/igvaeonic-nasal-spirant-law/parents")
@cache.cached(timeout=CACHE_TIMEOUT)
def handleIgvaeonicNSParents():
    page = request.args.get("page") or 1
    pageSize = request.args.get("pageSize") or 100

    # fromDescendant = request.args.get("fromDescendant").split(",")
    # toDescendant = request.args.get("toDescendant").split(",")

    igvaeonicParents = getIgvaeonicParents(int(page), int(pageSize))

    return jsonify(igvaeonicParents)

@app.route("/igvaeonic-nasal-spirant-law/graph/<parentID>")
@cache.cached(timeout=CACHE_TIMEOUT)
def handleIgvaeonicNSLaw(parentID):
    # fromDescendant = request.args.get("fromDescendant").split(",")
    # toDescendant = request.args.get("toDescendant").split(",")

    igvaeonicLinks = getIgvaeonicLinks(int(parentID))

    return jsonify(igvaeonicLinks)

@app.route("/etymology/root/<rootLanguage>/<rootWord>")
@cache.cached(timeout=CACHE_TIMEOUT)
def handleEtymologyFromRoot(rootLanguage, rootWord):
    graph = getCommonAncestors(rootLanguage, rootWord)

    return jsonify(graph)


@app.route("/")
def index():
    return "Etymology yo!"

@app.route("/linguistics-api")
def debugging():
    return "Request to '/linguistics-api'!"

@app.errorhandler(404)
def page_not_found(e):
    return jsonify(error=str(e)), 404

# ----------------------------------






# ---------------------------------------

def getID(languageCode, latinTranscription):
    queryText = """
        match (a) 
        where a.latinTranscription=$latinTranscription
        and a.languageCode=$languageCode
        return id(a) as id"""
    
    bindVars = {
        "languageCode": languageCode,
        "latinTranscription": latinTranscription
    }
    
    with driver.session() as session:
        result = session.run(queryText, bindVars)
        record = result.single()
        return record["id"] if record else None



def getDoubletsForLanguage(language, limit=100):
    """ For a given language, return all distinct words that have doublets """
    print(f"Getting doublets for {language}")
    queryText = """
        match p=(a:word)<-[q:inherited|borrowed|derived *1..7]-()-[r:inherited|borrowed|derived *1..6]->(b:word)-[]->(c:word)
        where a.languageCode = $language
        and c.languageCode = $language
        and b.languageCode <> $language
        and a.latinTranscription <> c.latinTranscription
        and toLower(q.articleTitle) = a.latinTranscription
        and toLower(r.articleTitle) = c.latinTranscription
        return distinct c
        limit $limit"""
    bindVars = {
        "language": language,
        "limit": limit,
    }
    
    result = runCypherQuery(queryText, bindVars)
    doubletGroups = []
    for record in result:
        lt = record["c"].get("latinTranscription")
        doubletGroups.append(lt)

    return doubletGroups

def getDoubletsForWord(language, word):
    """ For a given word in a language, return all doublets for that word, and the graph linking this group of doublets """
    queryText = """
        match (a:word)<-[r:inherited|borrowed|derived *..7]-(b:word) 
        where a.latinTranscription=$word
        and a.languageCode=$language 
        with collect(distinct b) as nodes 
        unwind nodes as node 
        match p=(c:word)-[q:inherited|derived|borrowed *..7]->(d:word) 
        where d.languageCode=$language 
        and id(c)=id(node) 
        return nodes(p), relationships(p);
    """
    bindVars = {
        "language": language,
        "word": word
    }

    with driver.session() as session:
        result = session.run(queryText, bindVars)   
        resp = getGraphFromBoltGraph(result.graph())
        return resp

def autocomplete(languageCode, searchTerm, maximumWordLength=None):
    returnCompounds = False
    if maximumWordLength:
        maximumWordLength=int(maximumWordLength)

    # first check the cache
    firstTwoCharacters = searchTerm[0:2]
    cachedResult = list(
        freetextCache.find(
            {
                "searchTerm": firstTwoCharacters,
                "languageCode": languageCode
            }, {"_id": 0}))


    # if we've got a cached result, use it
    if len(cachedResult) > 0:
        if returnCompounds == False:
            simpleWords = list(filter(lambda r: r["isCompound"] ==
                                    False, cachedResult[0]["nodes"]))

            cachedResult[0]["nodes"] = simpleWords

        cachedResult = cachedResult[0]

        # update the cache
        newDate = datetime.datetime.now()
        freetextCache.update(
            {
                "searchTerm": firstTwoCharacters,
                "languageCode": languageCode
            }, {
                "$set": {
                    "lastDate": newDate
                },
                "$inc": {
                    "count": 1
                }
            })

        # get all cached nodes that start with our string
        matchingTerms = filter(
            lambda e: e["latinTranscription"][0:len(searchTerm)].lower() ==
            searchTerm.lower(), cachedResult["nodes"])
        if maximumWordLength:
            matchingTerms = filter(lambda e: len(e["latinTranscription"])<=maximumWordLength, matchingTerms)
        cachedResult["nodes"] = list(matchingTerms)
        cachedResult["searchTerm"] = searchTerm

        return cachedResult
    else:
        print("no matches mate")
        queryText = f"""match (a:word)
             where toLower(a.latinTranscription) starts with $searchTerm
             and toLower(a.languageCode)=toLower($languageCode)
            {' ' if returnCompounds else ' and NOT (a.latinTranscription contains " " or a.latinTranscription contains "-")' }
            {' and size(a.latinTranscription)<=$maximumWordLength' if maximumWordLength else ' ' }
             return a"""

        bindVars = {
            "languageCode": languageCode,
            "searchTerm": searchTerm,
            "maximumWordLength": maximumWordLength
        }

        result = runCypherQuery(queryText, bindVars)

        resp = getNodesFromBoltStatementResult(result)

        # insert into cache
        nodes = list(map(lambda e: {**e, 'doublets': 0, 'parents': 1, 'isCompound': False}, resp["nodes"]))
        cachedObject = resp
        cachedObject["nodes"] = nodes
        cachedObject["searchTerm"] = firstTwoCharacters
        cachedObject["languageCode"] = languageCode
        cachedObject["lastDate"] = datetime.datetime.now()
        cachedObject["isCompound"] = False
        cachedObject["doublets"] = 0
        cachedObject["parents"] = 0
        cachedObject["count"] = 1
        freetextCache.insert(cachedObject)

        resp["searchTerm"] = searchTerm
        try:
            del resp["_id"]
        except KeyError:
            pass

        return resp

def getLinks(parentCode, childCode, page=1, pageSize=100):
    queryText = """
            match p=(a:word)-[r:inherited|borrowed|derived *1..10]->(b:word)
            where b.languageCode = $childLanguageCode
            and a.languageCode = $parentLanguageCode
            and all (idx in range(0, size(r)-2) where (r[idx]).articleTitle = (r[idx+1]).articleTitle)
            return p
            skip $skip
            limit $limit"""
    bindVars = {
        "childLanguageCode": childCode,
        "parentLanguageCode": parentCode,
        "skip": ((page - 1)*pageSize),
        "limit": pageSize,
    }
    result = runCypherQuery(queryText, bindVars)

    with driver.session() as session:
        result = session.run(queryText, bindVars)   
        chains = []
        chainMap = []
        for record in result:
            chain = []
            firstItem = record["p"].relationships[0]

            # don't repeat words
            if firstItem.get("articleTitle") in chainMap:
                continue
            else:
                chainMap.append(firstItem.get("articleTitle"))

            for n in record["p"].nodes:
                node = {}
                for p in n.items():
                    node[p[0]] = p[1]
                chain.append(node)

            chains.append(chain)

        return chains

def getLinksWithSingleStep(parentCode, childCode, page=1, pageSize=300):
    queryText = """
        match p=(a:word)-[r:inherited|borrowed|derived]->(b:word)
        where b.languageCode = $childLanguageCode
        and a.languageCode = $parentLanguageCode
        return distinct p
        skip $skip
        limit $limit"""
    bindVars = {
        "childLanguageCode": childCode,
        "parentLanguageCode": parentCode,
        "skip": ((page - 1)*pageSize),
        "limit": pageSize,
    }

    with driver.session() as session:
        result = session.run(queryText, bindVars)   
        resp = getGraphFromBoltGraph(result.graph())
        return resp

def getChildrenOfTwoNodes(parent, child, fromDescendant, toDescendant):
    childLangQuery = """
        match (a:word) 
        where id(a)=$id
        return a.languageCode as languageCode
        """
    clqBindVars = {"id": int(child)}
    with driver.session() as session:
        clqResult = session.run(childLangQuery, clqBindVars)   

        for record in clqResult:
            childLanguageCode = record["languageCode"]
        

        originalLinkQuery = """
            match p=(parent:word)-[r:inherited|borrowed|derived]->(child:word)
            where id(parent)=$parentID
            and id(child)=$childID
            return p
            limit 1
            """

        bindVars = {
            "parentID": int(parent),
            "childID": int(child),
            "childLanguageCode": childLanguageCode,
            "fromDescendant": fromDescendant, 
            "toDescendant": toDescendant
        }

        parentDescendantQuery = """
            match p=(descendant:word)<-[q:inherited|borrowed|derived *..4]-(otherChild:word)<-[r:inherited|borrowed|derived]-(parent:word)
            where id(parent)=$parentID
            and id(otherChild)<>$childID
            and otherChild.languageCode<>$childLanguageCode
            and descendant.languageCode=$fromDescendant
            return p
            limit 2
            """

        childDescendantQuery = """
            match p=(child:word)-[r:inherited|borrowed|derived *..4]->(descendant:word)
            where id(child)=$childID
            and descendant.languageCode=$toDescendant
            return p
            limit 2
            """

        originalResult = session.run(originalLinkQuery, bindVars)
        parentResult = session.run(parentDescendantQuery, bindVars)
        childResult = session.run(childDescendantQuery, bindVars)
        
        originalGraph = getGraphFromBoltGraph(originalResult.graph())
        parentGraph = getGraphFromBoltGraph(parentResult.graph())
        childGraph = getGraphFromBoltGraph(childResult.graph())

        return {'nodes': originalGraph["nodes"] + parentGraph["nodes"] + childGraph["nodes"], 'links': originalGraph["links"] + parentGraph["links"] + childGraph["links"]}

def getGrassmanLinks(parent, child):
    aspiratedConsonants = ["bʰ", "dʰ", "gʰ","pʰ", "tʰ", "kʰ"]
    aspiratedMap = {"bʰ": "b", "dʰ": "d", "gʰ": "g","pʰ": "p", "tʰ": "t", "kʰ": "k"}
    pieToIndoQuery = """
        match p=(a:word)<-[r:inherited|borrowed|derived]-(b:word)
        where b.languageCode = $parent
        and a.languageCode = $child
        return distinct p"""

    pieToIndoBindVars = {
        "parent": parent,
        "child": child
    }
    with driver.session() as session:

        pieToIndoResults = session.run(pieToIndoQuery, pieToIndoBindVars)

        pathsWithTwoAspirates = []
        for record in pieToIndoResults:
            path = record.values()[0]
            relationship = path.relationships[0]
            progressions = list(map(lambda e: e.split(">"), relationship["progressions"].split(",")))
            aspiratedProgressions = list(filter(lambda e: e[0] in aspiratedConsonants, progressions))
            if len(aspiratedProgressions) == 2 and aspiratedProgressions[0][1] == aspiratedMap[aspiratedProgressions[0][0]]:
                pathsWithTwoAspirates.append({"link": serialiseLink(relationship), "nodes": [serialiseNode(path.start_node), serialiseNode(path.end_node)]})
            
        return pathsWithTwoAspirates  

def getIgvaeonicParents(page=1, pageSize=2500):
    vowels = ["a", "e", "i", "o", "u"]
    fricatives = ["s", "f", "th", "þ"]
    igvaeonicQuery = """
        match p=(a:word)-[r:inherited|borrowed|derived]->(b:word)
        where a.languageCode in ["gem-pro", "gmw-pro"]
        and b.languageCode in ["ang", "odt", "ofs", "osx"]
        return distinct a, r
        skip $skip
        limit $limit"""

    bindvars = {
        "skip": (page-1) * pageSize,
        "limit": pageSize
    }

    with driver.session() as session:
        results = session.run(igvaeonicQuery, bindvars)
        nodes = []
        for record in results:
            progressions = list(map(lambda e: e.split(">"), record["r"].get("progressions").split(",")))
            parent=serialiseNode(record["a"])
            try:
                try:
                    disappearingNasal = progressions.index(["n", "-"])
                except (IndexError, ValueError):
                    try:
                        disappearingNasal = progressions.index(["m", "-"])
                    except (IndexError, ValueError): 
                        continue
                        
                preceededByVowel = progressions[disappearingNasal - 1][0] in vowels
                proceededByFricative = progressions[disappearingNasal + 1][1] in fricatives
                if preceededByVowel and proceededByFricative and parent not in nodes:
                    nodes.append(parent)
            except (IndexError, ValueError):
                pass

        return nodes 

def getIgvaeonicLinks(parentID):
    igvaeonicQueries = """
        match p=(a:word)-[:inherited|borrowed|derived]->(b:word)
        where id(a)=$parentID
        and b.languageCode in ["ang", "odt", "ofs", "osx", "goh", "got"]
        optional match s=(b:word)-[:inherited|borrowed|derived *..5]->(c:word)
        where  c.languageCode in ["en", "nl", "nds", "nds-de", "frr", "fy", "stq", "de", "gme-cgo"]
        and b.languageCode in ["ang", "odt", "ofs", "osx"]
        return nodes(p), relationships(p),nodes(s), relationships(s)"""
     
    bindVars = {
        "parentID": parentID,
    }
    with driver.session() as session:

        result = session.run(igvaeonicQueries, bindVars)

        graph = getGraphFromBoltGraph(result.graph())

        return graph


def getCommonAncestors(rootLanguage, rootWord):
    cypherQuery = f"""match p=(a:word)<-[r:inherited *..7]-(b:word) 
        where b.languageCode=$languageCode
        and b.latinTranscription=$latinTranscription
        return nodes(p) as nodes, relationships(p) as links
    """
    
    bindVars = {
        "languageCode": rootLanguage,
        "latinTranscription": rootWord,
    }

    with driver.session() as session:
        result = session.run(cypherQuery, bindVars)   
        graph = getGraphFromBoltGraph(result.graph())
        return graph




if __name__ == "__main__":
    app.run()