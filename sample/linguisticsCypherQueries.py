# from . import runCypherQuery, getNodesFromBoltStatementResult, serialiseLink, serialiseNode
from flask import Flask, jsonify,abort
from neo4j import GraphDatabase
from pymongo import MongoClient
from flask_cors import CORS
import datetime

app = Flask(__name__)
CORS(app)


client = MongoClient('localhost', 27017)
linguisticsDB = client.linguistics

MongoClient = MongoClient('localhost', 27017)
cacheDB = MongoClient.cache
freetextCache = cacheDB["etymologyWords"]


def getDoubletsForLanguage(language, limit=100):
    """ For a given language, return all distinct words that have doublets """
    print(f"Getting doublets for {language}")
    queryText = """
        match p=(a:word)<-[q:inherited|borrowed|derived *1..7]-()-[r:inherited|borrowed|derived *1..6]->(b:word)-[]->(c:word)
        where a.languageCode = $language
        and c.languageCode = $language
        and b.languageCode <> $language
        and a.latinTranscription <> c.latinTranscription
        and q.articleTitle = a.latinTranscription
        and r.articleTitle = c.latinTranscription
        return distinct c
        limit $limit"""
    bindVars = {
        "language": language,
        "limit": limit,
    }
    
    result = runCypherQuery(queryText, bindVars)
    doubletGroups = []
    for record in result:
        lt = record["c"].get("latinTranscription")
        doubletGroups.append(lt)

    return doubletGroups

def getDoubletsForWord(language, word):
    """ For a given word in a language, return all doublets for that word, and the graph linking this group of doublets """
    queryText = """
        match (a:word)<-[r:inherited|borrowed|derived *..7]-(b:word) 
        where a.latinTranscription=$word
        and a.languageCode=$language 
        with collect(distinct b) as nodes 
        unwind nodes as node 
        match p=(c:word)-[q:inherited|derived|borrowed *..7]->(d:word) 
        where d.languageCode=$language 
        and id(c)=id(node) 
        return nodes(p), relationships(p);
    """
    bindVars = {
        "language": language,
        "word": word
    }
    
    result = runCypherQuery(queryText, bindVars)

    resp = getNodesFromBoltStatementResult(result)


    return resp

def autocomplete(languageCode, searchTerm, maximumWordLength=None):
    returnCompounds = False
    if maximumWordLength:
        maximumWordLength=int(maximumWordLength)

    # first check the cache
    firstTwoCharacters = searchTerm[0:2]
    cachedResult = list(
        freetextCache.find(
            {
                "searchTerm": firstTwoCharacters,
                "languageCode": languageCode
            }, {"_id": 0}))


    # if we've got a cached result, use it
    if len(cachedResult) > 0:
        if returnCompounds == False:
            simpleWords = list(filter(lambda r: r["isCompound"] ==
                                    False, cachedResult[0]["nodes"]))

            cachedResult[0]["nodes"] = simpleWords

        cachedResult = cachedResult[0]

        # update the cache
        newDate = datetime.datetime.now()
        freetextCache.update(
            {
                "searchTerm": firstTwoCharacters,
                "languageCode": languageCode
            }, {
                "$set": {
                    "lastDate": newDate
                },
                "$inc": {
                    "count": 1
                }
            })

        # get all cached nodes that start with our string
        matchingTerms = filter(
            lambda e: e["latinTranscription"][0:len(searchTerm)].lower() ==
            searchTerm.lower(), cachedResult["nodes"])
        if maximumWordLength:
            matchingTerms = filter(lambda e: len(e["latinTranscription"])<=maximumWordLength, matchingTerms)
        cachedResult["nodes"] = list(matchingTerms)
        cachedResult["searchTerm"] = searchTerm

        return cachedResult
    else:
        print("no matches mate")
        queryText = f"""match (a:word)
             where toLower(a.latinTranscription) starts with $searchTerm
             and toLower(a.languageCode)=toLower($languageCode)
            {' ' if returnCompounds else ' and NOT (a.latinTranscription contains " " or a.latinTranscription contains "-")' }
            {' and size(a.latinTranscription)<=$maximumWordLength' if maximumWordLength else ' ' }
             return a"""

        bindVars = {
            "languageCode": languageCode,
            "searchTerm": searchTerm,
            "maximumWordLength": maximumWordLength
        }

        result = runCypherQuery(queryText, bindVars)

        resp = getNodesFromBoltStatementResult(result)

        # insert into cache
        nodes = list(map(lambda e: {**e, 'doublets': 0, 'parents': 1, 'isCompound': False}, resp["nodes"]))
        cachedObject = resp
        cachedObject["nodes"] = nodes
        cachedObject["searchTerm"] = firstTwoCharacters
        cachedObject["languageCode"] = languageCode
        cachedObject["lastDate"] = datetime.datetime.now()
        cachedObject["isCompound"] = False
        cachedObject["doublets"] = 0
        cachedObject["parents"] = 0
        cachedObject["count"] = 1
        freetextCache.insert(cachedObject)

        resp["searchTerm"] = searchTerm
        try:
            del resp["_id"]
        except KeyError:
            pass

        return resp

def getLinks(parentCode, childCode, page=1, pageSize=100):
    queryText = """
            match p=(a:word)-[r:inherited|borrowed|derived *1..10]->(b:word)
            where b.languageCode = $childLanguageCode
            and a.languageCode = $parentLanguageCode
            and all (idx in range(0, size(r)-2) where (r[idx]).articleTitle = (r[idx+1]).articleTitle)
            return p
            skip $skip
            limit $limit"""
    bindVars = {
        "childLanguageCode": childCode,
        "parentLanguageCode": parentCode,
        "skip": ((page - 1)*pageSize),
        "limit": pageSize,
    }
    result = runCypherQuery(queryText, bindVars)

    chains = []
    chainMap = []
    for record in result:
        chain = []
        firstItem = record["p"].relationships[0]

        # don't repeat words
        if firstItem.get("articleTitle") in chainMap:
            continue
        else:
            chainMap.append(firstItem.get("articleTitle"))

        for n in record["p"].nodes:
            node = {}
            for p in n.items():
                node[p[0]] = p[1]
            chain.append(node)

        chains.append(chain)

    return chains

def getLinksWithSingleStep(parentCode, childCode, page=1, pageSize=300):
    queryText = """
        match p=(a:word)-[r:inherited|borrowed|derived]->(b:word)
        where b.languageCode = $childLanguageCode
        and a.languageCode = $parentLanguageCode
        return distinct p
        skip $skip
        limit $limit"""
    bindVars = {
        "childLanguageCode": childCode,
        "parentLanguageCode": parentCode,
        "skip": ((page - 1)*pageSize),
        "limit": pageSize,
    }
    result = runCypherQuery(queryText, bindVars)
    resp = getNodesFromBoltStatementResult(result)

    return resp

def getChildrenOfTwoNodes(parent, child, fromDescendant, toDescendant):
    childLangQuery = """
        match (a:word) 
        where id(a)=$id
        return a.languageCode as languageCode
        """
    clqBindVars = {"id": int(child)}
    clqResult = runCypherQuery(childLangQuery, clqBindVars)
    for record in clqResult:
        childLanguageCode = record["languageCode"]
        

    originalLinkQuery = """
        match p=(parent:word)-[r:inherited|borrowed|derived]->(child:word)
        where id(parent)=$parentID
        and id(child)=$childID
        return p
        limit 1
        """

    bindVars = {
        "parentID": int(parent),
        "childID": int(child),
        "childLanguageCode": childLanguageCode,
        "fromDescendant": fromDescendant, 
        "toDescendant": toDescendant
    }

    parentDescendantQuery = """
        match p=(descendant:word)<-[q:inherited|borrowed|derived *..4]-(otherChild:word)<-[r:inherited|borrowed|derived]-(parent:word)
        where id(parent)=$parentID
        and id(otherChild)<>$childID
        and otherChild.languageCode<>$childLanguageCode
        and descendant.languageCode=$fromDescendant
        return p
        limit 2
        """

    childDescendantQuery = """
        match p=(child:word)-[r:inherited|borrowed|derived *..4]->(descendant:word)
        where id(child)=$childID
        and descendant.languageCode=$toDescendant
        return p
        limit 2
        """

    originalResult = runCypherQuery(originalLinkQuery, bindVars)
    parentResult = runCypherQuery(parentDescendantQuery, bindVars)
    childResult = runCypherQuery(childDescendantQuery, bindVars)
    
    originalGraph = getNodesFromBoltStatementResult(originalResult)
    parentGraph = getNodesFromBoltStatementResult(parentResult)
    childGraph = getNodesFromBoltStatementResult(childResult)

    return {'nodes': originalGraph["nodes"] + parentGraph["nodes"] + childGraph["nodes"], 'links': originalGraph["links"] + parentGraph["links"] + childGraph["links"]}

def getGrassmanLinks(parent, child):
    aspiratedConsonants = ["bʰ", "dʰ", "gʰ","pʰ", "tʰ", "kʰ"]
    aspiratedMap = {"bʰ": "b", "dʰ": "d", "gʰ": "g","pʰ": "p", "tʰ": "t", "kʰ": "k"}
    pieToIndoQuery = """
        match p=(a:word)<-[r:inherited|borrowed|derived]-(b:word)
        where b.languageCode = $parent
        and a.languageCode = $child
        return distinct p"""

    pieToIndoBindVars = {
        "parent": parent,
        "child": child
    }

    pieToIndoResults = runCypherQuery(pieToIndoQuery, pieToIndoBindVars)

    pathsWithTwoAspirates = []
    for record in pieToIndoResults:
        path = record.values()[0]
        relationship = path.relationships[0]
        progressions = list(map(lambda e: e.split(">"), relationship["progressions"].split(",")))
        aspiratedProgressions = list(filter(lambda e: e[0] in aspiratedConsonants, progressions))
        if len(aspiratedProgressions) == 2 and aspiratedProgressions[0][1] == aspiratedMap[aspiratedProgressions[0][0]]:
            pathsWithTwoAspirates.append({"link": serialiseLink(relationship), "nodes": [serialiseNode(path.start_node), serialiseNode(path.end_node)]})
        
    return pathsWithTwoAspirates  

def getIgvaeonicParents(page=1, pageSize=2500):
    vowels = ["a", "e", "i", "o", "u"]
    fricatives = ["s", "f", "th", "þ"]
    igvaeonicQuery = """
        match p=(a:word)-[r:inherited|borrowed|derived]->(b:word)
        where a.languageCode in ["gem-pro", "gmw-pro"]
        and b.languageCode in ["ang", "odt", "ofs", "osx"]
        return distinct a, r
        skip $skip
        limit $limit"""

    bindvars = {
        "skip": (page-1) * pageSize,
        "limit": pageSize
    }

    results = runCypherQuery(igvaeonicQuery, bindvars)
    nodes = []
    for record in results:
        progressions = list(map(lambda e: e.split(">"), record["r"].get("progressions").split(",")))
        parent=serialiseNode(record["a"])
        try:
            try:
                disappearingNasal = progressions.index(["n", "-"])
            except (IndexError, ValueError):
                try:
                    disappearingNasal = progressions.index(["m", "-"])
                except (IndexError, ValueError): 
                    continue
                    
            preceededByVowel = progressions[disappearingNasal - 1][0] in vowels
            proceededByFricative = progressions[disappearingNasal + 1][1] in fricatives
            if preceededByVowel and proceededByFricative and parent not in nodes:
                nodes.append(parent)
        except (IndexError, ValueError):
            pass

    return nodes 

def getIgvaeonicLinks(parentID):
    igvaeonicQueries = """
        match p=(a:word)-[:inherited|borrowed|derived]->(b:word)
        where id(a)=$parentID
        and b.languageCode in ["ang", "odt", "ofs", "osx", "goh", "got"]
        optional match s=(b:word)-[:inherited|borrowed|derived *..5]->(c:word)
        where  c.languageCode in ["en", "nl", "nds", "nds-de", "frr", "fy", "stq", "de", "gme-cgo"]
        and b.languageCode in ["ang", "odt", "ofs", "osx"]
        return nodes(p), relationships(p),nodes(s), relationships(s)"""
     
    bindVars = {
        "parentID": parentID,
    }

    result = runCypherQuery(igvaeonicQueries, bindVars)

    graph = getNodesFromBoltStatementResult(result)

    return graph

def getCommonAncestors(rootID, *childIDs):
    cypherQuery = """
        match p=(a:word)<-[r:inherited|borrowed|derived *..7]-(b:word) 
        where id(b)=$rootID and id(a) in $childIDs
        return nodes(p), relationships(p)
    """
    
    bindVars = {
        "rootID": rootID,
        "childIDs": childIDs
    }

    result = runCypherQuery(igvaeonicQueries, bindVars)

    graph = getNodesFromBoltStatementResult(result)

    return graph

