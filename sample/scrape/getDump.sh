# This script downloads wiktionary's latest data dump, and breaks the ~7Gb .xml file
# into chunks, where each chunk contains 9,999 articles


# get the latest dump
wget https://dumps.wikimedia.org/enwiktionary/latest/enwiktionary-latest-pages-articles.xml.bz2

# unzip
bzip2 -d enwiktionary-latest-pages-articles.xml.bz2

# copy the file so we keep the original in case the script fails
cp enwiktionary-latest-pages-articles.xml remaining.xml

# make up to 2000 files, each with 9,999 articles
for i in {1..2000};    
   do 
    echo $i;
    # split the file wherever we find <page>
    # k - keep files even when errors
    # s - silent
    # -n 4 use 4 digits for filename (i.e. up to part.9999)
    # -f part. - set the file prefix to "part.""
   csplit -ks -n 4 -f part. remaining.xml /\<page\>/ {9999} 2>/dev/null;
   # rename the leftover file 
   mv -f part.9999 remaining.xml; 
    #  concat all remaining files into one large xml doc
   cat part.* > $i.xml;
 done